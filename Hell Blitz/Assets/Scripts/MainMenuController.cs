﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenuController : MonoBehaviour
{ 
    public void Startup()
    {
        SceneManager.LoadScene(1);
    }
    public void Ending()
    {
        Application.Quit();
        UnityEditor.EditorApplication.isPlaying = false;
    }

   
}
